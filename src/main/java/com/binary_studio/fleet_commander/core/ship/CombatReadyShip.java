package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private final PositiveInteger initialShieldHP;

	private final PositiveInteger initialHullHP;

	private final PositiveInteger capacity;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private DefenciveSubsystem defensiveSubsystem;

	private AttackSubsystem attackSubsystem;

	private CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.initialShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.initialHullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacity = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defensiveSubsystem = defenciveSubsystem;
	}

	public static CombatReadyShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		return new CombatReadyShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate,
				speed, size, attackSubsystem, defenciveSubsystem);
	}

	@Override
	public void endTurn() {
		recap();
	}

	private void recap() {
		Integer newCap = this.capacitorAmount.value() + this.capacitorRechargeRate.value();
		this.capacitorAmount = PositiveInteger.of(Math.min(newCap, this.capacity.value()));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int diff = this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value();
		if (diff < 0) {
			return Optional.empty();
		}
		PositiveInteger damage = this.attackSubsystem.attack(target);
		this.capacitorAmount = PositiveInteger.of(diff);
		return Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		Integer diff = this.shieldHP.value() - attack.damage.value();
		if (diff > 0) {
			this.shieldHP = PositiveInteger.of(diff);
		}
		else {
			Integer diff2 = this.hullHP.value() + diff;
			this.shieldHP = PositiveInteger.of(0);
			if (diff2 < 0) {
				return new AttackResult.Destroyed();
			}
			this.hullHP = PositiveInteger.of(diff2);
		}
		attack = this.defensiveSubsystem.reduceDamage(attack);
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Integer diff = this.capacitorAmount.value() - this.defensiveSubsystem.getCapacitorConsumption().value();
		if (diff < 0) {
			return Optional.empty();
		}
		this.capacitorAmount = PositiveInteger.of(diff);
		var maxReg = this.defensiveSubsystem.regenerate();
		Integer shSum = maxReg.shieldHPRegenerated.value() + this.shieldHP.value();
		Integer sh = maxReg.shieldHPRegenerated.value();
		if (this.initialShieldHP.value() < shSum) {
			sh = this.initialShieldHP.value() - this.shieldHP.value();
		}
		Integer hlSum = maxReg.hullHPRegenerated.value() + this.hullHP.value();
		Integer hl = maxReg.hullHPRegenerated.value();
		if (this.initialHullHP.value() < hlSum) {
			hl = this.initialHullHP.value() - this.hullHP.value();
		}
		var res = new RegenerateAction(PositiveInteger.of(sh), PositiveInteger.of(hl));
		return Optional.of(res);
	}

}
