package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private DefenciveSubsystem defensiveSubsystem;

	private AttackSubsystem attackSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}
		int rest = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
		if (rest >= 0) {
			this.powergridOutput = PositiveInteger.of(rest);
			this.attackSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException(Math.abs(rest));
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defensiveSubsystem = null;
			return;
		}
		int rest = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
		if (rest >= 0) {
			this.powergridOutput = PositiveInteger.of(rest);
			this.defensiveSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException(Math.abs(rest));
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defensiveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.defensiveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		return CombatReadyShip.construct(this.name, this.shieldHP, this.hullHP, this.powergridOutput,
				this.capacitorAmount, this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem,
				this.defensiveSubsystem);
	}

}
