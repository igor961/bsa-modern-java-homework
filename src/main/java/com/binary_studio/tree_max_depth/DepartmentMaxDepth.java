package com.binary_studio.tree_max_depth;

import java.util.*;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		int maxDepth = 0;
		Stack<AbstractMap.SimpleImmutableEntry<Department, Integer>> tmpList = new Stack<>();
		tmpList.add(createPair(rootDepartment, 0));
		int tmpMaxDepth = 0;
		while (!tmpList.isEmpty()) {
			var curEntry = tmpList.pop();
			Department current = curEntry.getKey();
			tmpMaxDepth = curEntry.getValue();
			if (current != null) {
				maxDepth = Math.max(tmpMaxDepth + 1, maxDepth);
				if (!current.subDepartments.isEmpty()) {
					for (var el : current.subDepartments) {
						tmpList.push(createPair(el, tmpMaxDepth + 1));
					}
				}
			}
		}
		return maxDepth;
	}

	private static <T> AbstractMap.SimpleImmutableEntry<T, Integer> createPair(T d, int i) {
		return new AbstractMap.SimpleImmutableEntry<>(d, i);
	}

}
